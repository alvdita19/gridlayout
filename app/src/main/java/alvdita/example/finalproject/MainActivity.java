package alvdita.example.finalproject;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    //Inisialisasi Pertama di Java
    FrameLayout btnprofi1;
    FrameLayout btnmycity1;
    FrameLayout btnmyeducation1;
    FrameLayout btnmyfamily1;
    FrameLayout btnquiz1;
    FrameLayout btnquit1;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inisialisasi Kedua di Java
        btnprofi1 = (FrameLayout) findViewById(R.id.btn_profil);
        btnmycity1 = (FrameLayout) findViewById(R.id.btn_city);
        btnmyeducation1 = (FrameLayout) findViewById(R.id.btn_education);
        btnmyfamily1 = (FrameLayout) findViewById(R.id.btn_family);
        btnquiz1 = (FrameLayout) findViewById(R.id.btn_quiz);
        btnquit1 = (FrameLayout) findViewById(R.id.btn_close);
        long lastPress;
        Toast backpressToast;

        //Button Profil Ketika DiKlik
        btnprofi1.setOnClickListener(new View.OnClickListener()
                                     {
                                         @Override
                                         public void onClick(View v)
                                         {
                                             Toast.makeText(getApplicationContext(), "Profil Telah Dipilih", Toast.LENGTH_SHORT).show();
                                             Intent beach = new Intent(MainActivity.this, MyProfile.class);
                                             startActivity(beach);
                                         }
                                     }
        );

        //Button MyCity Ketika DiKlik
        btnmycity1.setOnClickListener(new View.OnClickListener()
                                      {
                                          @Override
                                          public void onClick(View v)
                                          {
                                              Toast.makeText(getApplicationContext(), "MyCity Telah Dipilih", Toast.LENGTH_SHORT).show();
                                              Intent beach = new Intent(MainActivity.this, MyCity.class);
                                              startActivity(beach);
                                          }
                                      }
        );

        //Button MyEducation Ketika DiKlik
        btnmyeducation1.setOnClickListener(new View.OnClickListener()
                                           {
                                               @Override
                                               public void onClick(View v)
                                               {
                                                   Toast.makeText(getApplicationContext(), "MyEducation Telah Dipilih", Toast.LENGTH_SHORT).show();
                                                   Intent beach = new Intent(MainActivity.this, MyEducation.class);
                                                   startActivity(beach);
                                               }
                                           }
        );

        //Button MyFamily Ketika DiKlik
        btnmyfamily1.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View v)
                                            {
                                                Toast.makeText(getApplicationContext(), "MyFamily Telah Dipilih", Toast.LENGTH_SHORT).show();
                                                Intent beach = new Intent(MainActivity.this, MyFamily.class);
                                                startActivity(beach);
                                            }
                                        }
        );

        //Button Quiz Ketika DiKlik
        btnquiz1.setOnClickListener(new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {
                                            Toast.makeText(getApplicationContext(), "Quiz Telah Dipilih", Toast.LENGTH_SHORT).show();
                                            Intent beach = new Intent(MainActivity.this, Quiz.class);
                                            startActivity(beach);
                                        }
                                    }
        );

        //Button Exit Ketika DiKlik
        btnquit1.setOnClickListener(new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {
                                            Toast.makeText(getApplicationContext(), "Aplikasi Keluar", Toast.LENGTH_SHORT).show();
                                            finish();
                                            System.exit(0);
                                        }
                                    }
        );


    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();

    }
}
package alvdita.example.finalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Quiz extends AppCompatActivity {
    EditText ed1;
    TextView tv1, tv2, tv3;
    RadioButton a, b, c, d;
    Button bt;
    RadioGroup rg;
    int q, s;
    ImageView image_smile, image_sad, image_angry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        ed1 = (EditText) findViewById(R.id.name);
        tv1 = (TextView) findViewById(R.id.ques);
        tv2 = (TextView) findViewById(R.id.response);
        tv3 = (TextView) findViewById(R.id.score);
        rg = (RadioGroup) findViewById(R.id.optionGroup);
        a = (RadioButton) findViewById(R.id.option1);
        b = (RadioButton) findViewById(R.id.option2);
        c = (RadioButton) findViewById(R.id.option3);
        d = (RadioButton) findViewById(R.id.option4);
        bt = (Button) findViewById(R.id.next);
        image_smile = (ImageView) findViewById(R.id.img_smile);
        image_angry = (ImageView) findViewById(R.id.img_angry);
        image_sad = (ImageView) findViewById(R.id.img_sad);
        q = 0;
        s = 0;

    }

    public void quiz(View v){
        switch (q){
            case 0:
            {
                ed1.setVisibility(View.GONE);
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                tv2.setText("");
                tv3.setText("");
                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(true);
                bt.setText("Next");
                s=0;

                tv1.setText("1. Dalam menyusun suatu program,langkah pertama yang harus di lakkukan adalah ?");
                a.setText("Membut Program");
                b.setText("Membuat Algoritma");
                c.setText("membeli Komputer");
                d.setText("Mempelajari Program");
                q = 1;
                break;
            }
            case 1: {
                ed1.setEnabled(false);
                tv1.setText("2. Tari saman merupakan tarian dari daerah ?");
                a.setText("Bengkulu");
                b.setText("Aceh");
                c.setText("Padang");
                d.setText("Makassar");

                if (b.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!b.isChecked()) {
                    SalahJawab();
                }
                q = 2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 2: {
                tv1.setText("3. Sebuah sistem informasi manajemen mengandung elemen-elemen fisik ?");
                a.setText("Pemasaran");
                b.setText("Pengorganisasian");
                c.setText("Perangkat Keras");
                d.setText("Pencatat Administrasi");
                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 3: {

                tv1.setText("4. Pada pembuatan program komputer, algoritma dibuat?");
                a.setText("Sebelum pembuatan program");
                b.setText("Pada Saat program dibuat");
                c.setText("Sesudah pembuatan program");
                d.setText("Pada saat program dijalankan");
                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 4: {
                tv1.setText("5. Tahapan dalam menyelesaikan suatu masalah adalah ?");
                a.setText("Masalah-Pseudocode-Flowchart-Program-Eksekusi-Hasil");
                b.setText("Masalah-Algoritma-Flowchart-Program-Eksekusi-Hasil");
                c.setText("Masalah-Model-Algoritma-Eksekusi-Hasil");
                d.setText("Masalah-Model-Algoritma-Program-Eksekusi-hasil ");
                if (a.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!a.isChecked()) {
                    SalahJawab();
                }
                q = 5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 5: {
                tv1.setText("6. +i dalam sistem informasi manajemen mencakup tersebut di bawah ini, kecuali . . .");
                a.setText("Komputer");
                b.setText("Orang");
                c.setText("Memory");
                d.setText("Database");

                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 6: {
                tv1.setText("7. Cang bertugas $enghidupkan dan mematikan mesin serta melakukan pemeliharaan sistem komputer adalah ?");
                a.setText("Trainer");
                b.setText("System Analyst");
                c.setText("Operator");
                d.setText("Teknisi Komputer");

                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 7: {
                tv1.setText("8. Cang bertugas mengelola proyek pembangunan software adalah ?");
                a.setText("Peneliti");
                b.setText("Project Manager");
                c.setText("System Analyst");
                d.setText("Operator");

                if (b.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!b.isChecked()) {
                    SalahJawab();
                }
                q = 8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 8: {
                tv1.setText("9. Beberapa jenis ilmu yang dipelajari dan dipakai dalam penerapan K3, kecuali ?");
                a.setText("alam");
                b.setText("perilaku");
                c.setText("agama");
                d.setText("kesehatan");

                if (c.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!c.isChecked()) {
                    SalahJawab();
                }
                q = 9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 9: {
                tv1.setText("10. Contoh parameter untuk mengukur kwalitas udara, kecuali ?");
                a.setText("panas");
                b.setText("ventilasi");
                c.setText("kebisingan");
                d.setText("volume");

                if (d.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!d.isChecked()) {
                    SalahJawab();
                }
                q = 10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 10: {
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                if (b.isChecked()) {
                    JawabBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    TidakDiJawab();
                } else if (!b.isChecked()) {
                    SalahJawab();
                }
                tv3.setText(ed1.getText() + "'s final score is " + s);
                bt.setText("Restart");
                q = 0;
                break;
            }
        }
    }


    public void JawabBenar() {
        tv2.setText("RIGHT ANSWER !!");
        s = s + 50;
        image_smile.setVisibility(View.VISIBLE);
        image_smile.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_smile.setVisibility(View.INVISIBLE);
            }
        }, 1000);
    }

    public void TidakDiJawab() {
        tv2.setText("You Are not Answerd");
        s = s + 0;
        image_angry.setVisibility(View.VISIBLE);
        image_angry.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_angry.setVisibility(View.INVISIBLE);
            }
        }, 1000);
    }

    public void SalahJawab() {
        tv2.setText("SORRY.. You Wrong Answer ㅠ.ㅠ");
        s = s - 10;
        image_sad.setVisibility(View.VISIBLE);
        image_sad.postDelayed(new Runnable() {
            @Override
            public void run() {
                image_sad.setVisibility(View.INVISIBLE);
            }
        }, 1000);
    }
}
